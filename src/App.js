import logo from "./logo.svg";
import "./App.css";
import { useState } from "react";
import Switch from "./components/Switch";
import Lock from "./components/ Lock";

function App() {
  const [showLogo, setShowLogo] = useState(true);
  const [locked, setLocked] = useState(false);

  const changeLogo = () => {
    setShowLogo(!showLogo);
  };
  const changeLocks = () => {
    setLocked(!locked);
  };

  return (
    <div className="App">
      <header className="App-header">
        {showLogo && <img src={logo} className="App-logo" alt="logo" />}
        <Switch func={changeLogo} lock={locked} text={showLogo}></Switch>
        <Lock func={changeLocks} texto={locked}></Lock>
      </header>
    </div>
  );
}

export default App;
