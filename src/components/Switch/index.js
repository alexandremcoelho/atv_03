const Switch = (props) => {
  return (
    <>
      <button onClick={props.func} disabled={props.lock}>
        {props.text ? "ESCONDER" : "MOSTRAR"}
      </button>
    </>
  );
};

export default Switch;
