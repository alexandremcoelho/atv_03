const Lock = (props) => {
  return (
    <button onClick={props.func}>{props.texto ? "DESTRAVAR" : "TRAVAR"}</button>
  );
};

export default Lock;
